package com.lostcenturiadecks.api.enumeration;

public enum CardQuality {
    none,
    normal,
    rare,
    hero,
    legend,
}
