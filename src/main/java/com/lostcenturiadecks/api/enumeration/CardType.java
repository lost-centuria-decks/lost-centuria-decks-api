package com.lostcenturiadecks.api.enumeration;

public enum CardType {
    none,
    monster,
    spell,
}
