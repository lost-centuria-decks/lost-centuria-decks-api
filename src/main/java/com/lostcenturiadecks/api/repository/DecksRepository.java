package com.lostcenturiadecks.api.repository;

import com.lostcenturiadecks.api.model.Decks;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface DecksRepository extends JpaRepository<Decks, UUID> {
}