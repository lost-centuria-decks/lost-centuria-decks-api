package com.lostcenturiadecks.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LostCenturiaDecksApplication {

	public static void main(String[] args) {
		SpringApplication.run(LostCenturiaDecksApplication.class, args);
	}

}
