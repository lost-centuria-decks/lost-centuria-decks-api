package com.lostcenturiadecks.api.controller;

import com.lostcenturiadecks.api.model.Cards;
import com.lostcenturiadecks.api.repository.CardsRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@Tag(name = "Cards", description = "SW Lost Centuria Cards information")
public class CardsController {

    @Autowired
    private CardsRepository cardsRepository;

    @GetMapping(path = "/cards")
    @Operation(summary = "Get all cards")
    public List<Cards> getAllCards() {
        return cardsRepository.findAll();
    }

    @GetMapping(path = "/cards/{id}")
    @Operation(summary = "Get a card by ID")
    public Optional<Cards> getCardById(@PathVariable int id) throws NotFoundException {
        Optional<Cards> card = cardsRepository.findById(id);
        if(card.isEmpty()) throw new NotFoundException("Card with id " + id + " not found");
        return card;
    }
}
