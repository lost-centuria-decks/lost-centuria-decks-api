package com.lostcenturiadecks.api.controller;

import com.lostcenturiadecks.api.model.Decks;
import com.lostcenturiadecks.api.repository.DecksRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@Tag(name = "Decks", description = "SW Lost Centuria Decks information")
public class DecksController {

    @Autowired
    private DecksRepository decksRepository;

    @GetMapping(path = "/decks")
    @Operation(summary = "Get all decks")
    public List<Decks> getAllDecks() {
        return decksRepository.findAll();
    }
    
    @GetMapping(path = "/decks/{id}")
    @Operation(summary = "Get a deck by ID")
    public Optional<Decks> getDeckById(@PathVariable UUID id) throws NotFoundException {
        Optional<Decks> deck = decksRepository.findById(id);
        if(deck.isEmpty()) throw new NotFoundException("Deck with id " + id + " not found");
        return deck;
    }

    @PostMapping(path = "/decks")
    @Operation(summary = "Create a deck")
    public Decks createDeck(@RequestBody Decks deck) {
        return decksRepository.save(deck);
    }

    @DeleteMapping(path = "/decks/{id}")
    @Operation(summary = "Delete a deck by ID")
    public void deleteDeck(@PathVariable UUID id) {
        decksRepository.deleteById(id);
    }

    @PutMapping(path = "/decks/{id}")
    @Operation(summary = "Update a deck by ID")
    public Decks updateDeck(@RequestBody Decks newDeck, @PathVariable UUID id) {
        Decks currentDeck = decksRepository.getOne(id);
        currentDeck.setDeckName(newDeck.getDeckName() != null ? newDeck.getDeckName() : currentDeck.getDeckName());
        currentDeck.setDescription(newDeck.getDescription() != null ? newDeck.getDescription() : currentDeck.getDescription());
        currentDeck.setPicture(newDeck.getPicture() != null ? newDeck.getPicture() : currentDeck.getPicture());
        currentDeck.setCards(!newDeck.getCards().isEmpty() ? newDeck.getCards() : currentDeck.getCards());
        return decksRepository.save(currentDeck);
    }
}
