package com.lostcenturiadecks.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@Table(name = "decks")
public class Decks {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private UUID id;

    @Column(name = "deckname")
    private String deckName;

    @Column(name = "description")
    private String description;

    @Column(name = "picture")
    private UUID picture;

    @Column(name = "rate")
    private int rate;

    @Column(name = "avg_cost")
    private double avgCost;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinTable(name = "deck_cards",
            joinColumns = {
                    @JoinColumn(name = "deck_id", nullable = false, updatable = false)},
            inverseJoinColumns = {
                    @JoinColumn(name = "card_id", nullable = false, updatable = false)})
    private Set<Cards> cards = new HashSet<>();

    public Decks() {
    }
}