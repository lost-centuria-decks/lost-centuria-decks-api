package com.lostcenturiadecks.api.model;

import com.lostcenturiadecks.api.PostgreSQLEnumType;
import com.lostcenturiadecks.api.enumeration.CardQuality;
import com.lostcenturiadecks.api.enumeration.CardType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Getter
@Setter
@ToString
@Table(name = "cards")
@TypeDef(name = "pgsql_enum", typeClass = PostgreSQLEnumType.class)
public class Cards {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "cardname")
    private String cardname;

    @Column(name = "subname")
    private String subname;

    @Column(name = "abilityname")
    private String abilityname;

    @Column(name = "description")
    private String description;


    @Column(name = "picture")
    private UUID picture;

    @Column(name = "cost")
    private int cost;

    @Type(type = "pgsql_enum")
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "card_quality")
    private CardQuality quality;

    @Type(type = "pgsql_enum")
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "card_type")
    private CardType type;
}