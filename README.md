﻿
# Lost Centuria Decks REST API

This REST API provide access to **Summoner's War Lost Centuria** cards and allows you to create/update/delete decks.

## Install

    mvn install

## Run the app

    mvn spring-boot:run

# REST API

The REST API will run on port `3001` by default.

Access to OpenAPI  : `/api/swagger`

## Get list of Cards

### Request

`GET /api/cards`

This request will show all cards information.

### Response

```
[
  {
    "id": 1,
	"cardname": "Zaiross",
    "subname": "Dragon",
    "abilityname": "Fiery Breath",
    "description": "Attacks the frontline ennemies with a powerful...",
    "picture": "0bfa9b34-88ef-4dc4-a3ca-69119d3982cc",
    "cost": 5,
    "quality": "legend",
    "type": "monster"
  },
  {
    "id": 2,
    "cardname": "Ragdoll",
    "subname": "Dragon Knight",
    "abilityname": "Dragon's Fury",
    "description": "Attacks the last enemy who used a skill...",
    "picture": "e5b158f9-02df-405f-8ca7-81bbc8dd2e2c",
    "cost": 4,
    "quality": "legend",
    "type": "monster"
  }
]
```

## Get Card by ID

### Request

`GET /api/cards/{id}`

   This request will show information from a specific card found from it's ID.
   
### Example

`GET /api/cards/1`

### Response

```
{
  "id": 1,
  "cardname": "Zaiross",
  "subname": "Dragon",
  "abilityname": "Fiery Breath",
  "description": "Attacks the frontline ennemies with a powerful...",
  "picture": "0bfa9b34-88ef-4dc4-a3ca-69119d3982cc",
  "cost": 5,
  "quality": "legend",
  "type": "monster"
}
```

## Get list of Decks

### Request

`GET /api/decks`

This request will show all decks information.

### Response

```
[
  {
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "deckName": "A deck",
    "description": "A beautiful deck",
    "picture": "b4b8991d-bc3f-4cdf-84b4-8049bb588c7e",
    "rate": 2,
    "avgCost": 5,
    "cards": [
      {
        "id": 1,
        "cardname": "Zaiross",
        "subname": "Dragon",
        "abilityname": "Fiery Breath",
        "description": "Attacks the frontline ennemies with a powerful...",
        "picture": "0bfa9b34-88ef-4dc4-a3ca-69119d3982cc",
        "cost": 5,
        "quality": "legend",
        "type": "monster"
      },
      {
	    "id": 2,
	    "cardname": "Ragdoll",
	    "subname": "Dragon Knight",
	    "abilityname": "Dragon's Fury",
	    "description": "Attacks the last enemy who used a skill...",
	    "picture": "e5b158f9-02df-405f-8ca7-81bbc8dd2e2c",
	    "cost": 4,
	    "quality": "legend",
	    "type": "monster"
	  }
    ]
  },
  {
    "id": "6ca4c834-b8be-4150-a05d-28061a596007",
    "deckName": "Another deck",
    "description": "Another beautiful deck",
    "picture": "c00cb4de-0c59-4def-8e58-81a7f1ffffe4",
    "rate": 4,
    "avgCost": 5,
    "cards": [
      {
        "id": 1,
        "cardname": "Zaiross",
        "subname": "Dragon",
        "abilityname": "Fiery Breath",
        "description": "Attacks the frontline ennemies with a powerful...",
        "picture": "0bfa9b34-88ef-4dc4-a3ca-69119d3982cc",
        "cost": 5,
        "quality": "legend",
        "type": "monster"
      }
    ]
  }
]
```
## Get Deck by ID

### Request

`GET /api/decks/{uuid}`

   This request will show information from a specific deck found from it's ID.
   
### Example

`GET /api/deck/3fa85f64-5717-4562-b3fc-2c963f66afa6`

### Response

```
{
  "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "deckName": "A deck",
  "description": "A beautiful deck",
  "picture": "b4b8991d-bc3f-4cdf-84b4-8049bb588c7e",
  "rate": 2,
  "avgCost": 4.5,
  "cards": [
    {
      "id": 1,
      "cardname": "Zaiross",
      "subname": "Dragon",
      "abilityname": "Fiery Breath",
      "description": "Attacks the frontline ennemies with a powerful...",
      "picture": "0bfa9b34-88ef-4dc4-a3ca-69119d3982cc",
      "cost": 5,
      "quality": "legend"
      "type": "monster"
    },
    {
	  "id": 2,
	  "cardname": "Ragdoll",
	  "subname": "Dragon Knight",
	  "abilityname": "Dragon's Fury",
	  "description": "Attacks the last enemy who used a skill...",
	  "picture": "e5b158f9-02df-405f-8ca7-81bbc8dd2e2c",
	  "cost": 4,
	  "quality": "legend",
	  "type": "monster"
	}
  ]
}
```

## Update a Deck by ID

### Request

`PUT /api/decks/{uuid}`

This request provides the possibility to modify the content of a deck from it's ID.

### Example

`PUT /api/deck/3fa85f64-5717-4562-b3fc-2c963f66afa6`

### Response

```
{
  "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "deckName": "A modified deck",
  "description": "A beautiful modified deck",
  "picture": "b4b8991d-bc3f-4cdf-84b4-8049bb588c7e",
  "rate": 2,
  "avgCost": 5,
  "cards": [
    {
      "id": 1,
      "cardname": "Zaiross",
      "subname": "Dragon",
      "abilityname": "Fiery Breath",
      "description": "Attacks the frontline ennemies with a powerful...",
      "picture": "0bfa9b34-88ef-4dc4-a3ca-69119d3982cc",
      "cost": 5,
      "quality": "legend"
      "type": "monster"
    }
  ]
}
```

## Delete a Deck by ID

### Request

`DELETE /api/deck/{uuid}`

This request provides the possibility to delete a deck from it's ID.

### Example

`DELETE /api/deck/3fa85f64-5717-4562-b3fc-2c963f66afa6`

### Response

    200 OK

